TD2 - Boucles et fonctions
**************************


Exercice 2
==========

.. include::  exercices/td2/minimum.rst
.. include::  exercices/td2/moyenne.rst
.. include::  exercices/td2/ecartGrandPetit.rst

Exercice 3
==========

.. include::  exercices/td2/nbApparition_str.rst
.. include::  exercices/td2/position_str.rst
