Un nombre appartient à une liste
--------------------------------

Ecrire une fonction permettant de savoir si un nombre appartient à une liste. 


.. easypython:: exercices/td2/variantes/appartient_nb.py
   :language: python
   :uuid: 1231313
