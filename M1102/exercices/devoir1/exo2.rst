Exercice 2
----------

Paul Ochon a des difficultés le matin pour se réveiller. Pour essayer de ne
pas être en retard il a adopté le principe suivant : s’il lui reste plus d’une
heure et demi alors il prend un petit déjeuner complet, s’il lui reste moins
d’une demi heure il ne prend pas de dejeuner du tout et dans les autres cas il
prend juste un café.
Paul travaille du lundi au vendredi et doit partir à 8h de chez lui.
Le samedi Paul s’impose de partir faire les courses à 9h et le dimanche
il part faire son footing à 11h.
Pour aider Paul le matin, on vous demande de déterminer son petit déjeuner en
fonction du jour de la semaine et de l’heure de son lever.
Écrire une fonction permettant d'indiquer quel est le déjeuner pris par Paul. 
Pour se simplifier le calcul sur les heures, on les écrira en décimale : ainsi
8.5 signifie 8h30.

Par exemple si on est **lundi** et qu’il se lève à **6h**, le déjeuner est
**complet**.
Si on est **samedi** et qu’il se lève à **8.75** (c-à-d 8h45), le déjeuner est
**rien**.

    
.. easypython:: exercices/devoir1/exo2.py
   :language: python
   :uuid: 1231313
