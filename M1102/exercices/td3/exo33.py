entrees_visibles = [('bonjour',), 
                    ('informatique',), 
]

entrees_invisibles = [
    ('lundi',),
    ('informaticien',),
    ('',), 
    ('abracadabra',), 
]


@solution
def inverser2A2(ch) : 
    """
    cette fonction permet de construire une cahine à partir de ch en permuttant 
    les lettres consécutives 2 à 2
    attention au traitement de la fin de chaine 
    """
    res = ''
    for position in range(0, len(ch) - 1 , 2) : 
        res += ch[position + 1] + ch[position]
    if len(res) != len(ch) : 
             res += ch[len(ch) - 1]
    return res
