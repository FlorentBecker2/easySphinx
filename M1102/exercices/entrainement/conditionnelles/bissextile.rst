Année bissextile
----------------

D'après Wikipédia, une année est bissextile  :

   * si l'année est divisible par 4 et non divisible par 100, ou
   * si l'année est divisible par 400.


Ainsi, 2017 n'est pas bissextile. L'an 2008 était bissextile (divisible par 4 et non divisible par 100).
L'an 1900 n'était pas bissextile car divisible par 4 mais aussi par 100 et non divisible par 400.
L'an 2000 était bissextile car divisible par 400.

Écrire une fonction qui prend en paramètre une année et qui renvoie si l’année est bissextile ou non.
    
.. easypython:: exercices/entrainement/conditionnelles/bissextile.py
   :language: python
   :uuid: 1231313
