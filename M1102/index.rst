.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

************************************************************************************
DUT Informatique Orléans: M1102 Introduction à l'algorithmique et à la programmation 
************************************************************************************

Les sujets de TP:

.. toctree::
   :maxdepth: 2

   tp3
   chaines
   tp5


Les sujets de TD:

.. toctree::
   :maxdepth: 2

   td2
   td3
   td4
   td5
	      
Les anciens devoirs:

.. toctree::
   :maxdepth: 2

   devoir1


Entrainement :

.. toctree::
   :maxdepth: 2

   entrainement1
